#!/usr/bin/env python3

'''
USAGE
    deep_tmhmm [OPTION] [FILE...]

DESCRIPTION
    Detects transmembrane domain region using DeepTMHMM. Requires 
    pybiolib and additionnaly docker to run locally on WSL. By default,
    returns sequence names along with the number of detected 
    transmembrane regions.

OPTIONS
    --local
        Run locally.

    -f, --filter=EXPRESSION
        Display only sequences with the corresponding number of 
        transmembrane regions provided in the EXPRESSION. The EXPRESSION 
        can be either the exact number of TM regions provided as an 
        integer, a closed range provided as two integers separated by a 
        dash (min-max), or a minimum or a maximum value provided with 
        '<' or '>' (>min, <max).

    -g, --gff3
        The GFF3 output is redirected to the standard output (do not 
        apply the filtering).

    -t, --topology
        The topology of the sequence is redirected to the standard
        output (do not apply the filtering).
    
    -s, --save-dir=DIR
        Save all files in the provided directory. If this option is not 
        set, the output files are removed at the end of the run.

    --quiet
        Do not report the progress.

    --help
        Display this message

'''

import getopt, sys, fileinput, re, tempfile, biolib, os
from contextlib import redirect_stdout

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], 
                                       "f:gnts:", 
                                       ['local',
                                        'filter=', 
                                        'gff3',
                                        'topology',
                                        'quiet',
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-f', '--filter'):
                self["filter"] = read_expr(a)
            elif o in ('-g', '--gff3'):
                self["output_fmt"] = "gff3"
            elif o == '--local':
                self["local"] = True
            elif o in ('-t', '--topology'):
                self["output_fmt"] = "topology"
            elif o in ('-s', '--save-dir'):
                self["save_dir"] = a 
            elif o == "--quiet":
                self["quiet"] = True

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["filter"] = (0, float('inf'))
        self["local"] = False
        self["output_fmt"] = "summary"
        self["save_dir"] = None
        self["quiet"] = False

class NamedDirectory(object):
    def __init__(self, name):
        self.name = name
    def __enter__(self):
        return self.name
    def __exit__(self, type, value, traceback):
        pass

def read_expr(expr):
    if re.fullmatch(r"[0-9]+", expr):
        min = max = int(expr)
    elif re.fullmatch(r"<[0-9]+", expr):
        min, max = 0, int(expr[1:])
    elif re.fullmatch(r">[0-9]+", expr):
        min, max = int(expr[1:]), float('inf')
    elif re.fullmatch(r"[0-9]+-[0-9]+", expr):
        min, max = map(int, expr.split("-"))
        if min > max: min, max = max, min
    else:
        raise ValueError(f"Unrecognized expression: {repr(expr)}")
    return (min, max)

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args

    # turn on/off GPU usage
    #os.environ["BIOLIB_DOCKER_RUNTIME"] = "nvidia" if options["gpu"] else ""
        
    # load the tool
    tool = biolib.load("DTU/DeepTMHMM:1.0.24")
    biolib.utils.STREAM_STDOUT = True

    # RE patterns
    summary_p = re.compile(r"# (?P<seqname>.+) Number of predicted TMRs:"
                           r"\s*(?P<n>\d+)")

    # organize the main job...
    with tempfile.NamedTemporaryFile(mode="w+", encoding="utf-8") as f:
        
        # transfer input a the temporary file
        f.writelines( line for line in fileinput.input() )
        f.seek(0)

        # run DeepTMHMM
        with (tempfile.TemporaryDirectory() if options["save_dir"] is None 
              else NamedDirectory(options["save_dir"])) as d:
            
            # redirect the progress log
            progress_f = open(os.devnull, "w") if options["quiet"] else sys.stderr
            with redirect_stdout(progress_f):
                kwargs = dict(args=f"--fasta {f.name}")
                if options["local"] == True:
                    kwargs.update(machine="local")
                job = tool.cli(**kwargs)
            job.save_files(d)

            # retrieve the output
            if options["output_fmt"] == "gff3":
                with open(f"{d}/TMRs.gff3") as resultsf:
                    sys.stdout.write(resultsf.read())

            elif options["output_fmt"] == "topology":
                with open(f"{d}/predicted_topologies.3line") as resultsf:
                    sys.stdout.write(resultsf.read())

            elif options["output_fmt"] == "summary":
                with open(f"{d}/TMRs.gff3") as resultsf:
                    for line in resultsf:
                        m = summary_p.fullmatch(line.strip())
                        if m is not None:
                            seqname = m.group("seqname")
                            n = int(m.group("n"))
                            if n >= options["filter"][0] and n <= options["filter"][1]:
                                sys.stdout.write(f"{seqname}\t{n}\n")
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

