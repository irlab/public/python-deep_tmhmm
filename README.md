# python-deep_tmhmm



## Description

Detects transmembrane domain region using [DeepTMHMM](https://dtu.biolib.com/DeepTMHMM/). Can be run locally, using GPUs, if the computer is equipped with a CUDA-compatible NVIDIA graphical card and configured with all the dependencies [listed below](#requires). Otherwise, jobs are sent to a server hosted by the Technical University of Denmark, with a limitation in the number of sequences (around 500) and jobs per day.

## Requires

This script was only tested under WSL2/Ubuntu version 22, running on Windows 11, after setting up the following dependencies :

- [pybiolib](https://pypi.org/project/pybiolib/) in WSL2 (`pip install pybiolib`)

- [docker](https://docs.docker.com/get-started/get-docker/) in Windows, make sure WSL 2 backend is [activated](https://docs.docker.com/desktop/wsl/)

- [nvidia-docker](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) in WSL 2

- Enable GPU usage (add `export BIOLIB_DOCKER_RUNTIME="nvidia"` to .profile)

## Command line options

    --local
        Run locally.

    -f, --filter=EXPRESSION
        Display only sequences with the corresponding number of
        transmembrane regions provided in the EXPRESSION. The EXPRESSION
        can be either the exact number of TM regions provided as an
        integer, a closed range provided as two integers separated by a
        dash (min-max), or a minimum or a maximum value provided with
        '<' or '>' (>min, <max).

    -g, --gff3
        The GFF3 output is redirected to the standard output (do not
        apply the filtering).

    -t, --topology
        The topology of the sequence is redirected to the standard
        output (do not apply the filtering).

    -s, --save-dir=DIR
        Save all files in the provided directory. If this option is not
        set, the output files are removed at the end of the run.

    --quiet
        Do not report the progress.

    --help
        Display this message


